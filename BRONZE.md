#### Decision
1. My initial thought was to use collision detection and slowed down the pod when I was near the opponent's pod, I increase the radius of the pod to 600 units and check if both pods were touching or intersecting then slowed my pod down to a thrust value of 85. The results were alright but didn't get me through the league.
2. I thought of how to turn my pod more effectively like I see in Formula 1 races so tried to decrease the pod's thrust and start to turn the pod before the checkpoint. To do this, I needed an understanding of the checkpoints so I used a map (data structure) to map all the checkpoints during the first lap. Once the mapping was complete, I started to check the pod's radius against the checkpoint radius to reduce the thrust value of 85 and if the thrust value was at 85, I would utilize the next checkpoint's coordinate as the destination so that my pod will start to turn while the inertia will still bring the pod to the current checkpoint. This seemed to work well but also presented some problems which happens when the pod collided with the opponent's pod.
3. After spending quite a bit of time figuring out various thrust values when nearing the current checkpoint, I thought of reducing the thrust value decrementally instead so that the pod wouldn't lose too much speed when the next checkpoint was relatively straight which helped me through the bronze league.

#### Time Spent
1. 3 hours
2. 4 hours
3. 2 hours

#### Final Code
```c++
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>

using namespace std;

// Checkpoints coordinates
struct CheckPointCoordinates {
    int x;
    int y;
};

// Credit: https://www.geeksforgeeks.org/check-two-given-circles-touch-intersect/
bool circleIntersect(int x1, int y1, int x2,
           int y2, int r1, int r2)
{
    int distSq = (x1 - x2) * (x1 - x2) +
                 (y1 - y2) * (y1 - y2);
    int radSumSq = (r1 + r2) * (r1 + r2);
    if (distSq > radSumSq)
        return false;
    else
        return true;
}

int main()
{
    int thrust = 100;
    map<unsigned int, CheckPointCoordinates> mapCheckPoints;
    bool mapAllCheckPoints = false;
    bool useBoost = false;
    int currentMapIndex = 1;

    // game loop
    while (1) {
        int x;
        int y;
        int nextCheckpointX; // x position of the next check point
        int nextCheckpointY; // y position of the next check point
        int nextCheckpointDist; // distance to the next checkpoint
        int nextCheckpointAngle; // angle between your pod orientation and the direction of the next checkpoint
        cin >> x >> y >> nextCheckpointX >> nextCheckpointY >> nextCheckpointDist >> nextCheckpointAngle; cin.ignore();
        int opponentX;
        int opponentY;
        cin >> opponentX >> opponentY; cin.ignore();
        bool podFacingOppositeDirection = (nextCheckpointAngle > 90 || nextCheckpointAngle < -90);
        bool podCheckPointIntersection = circleIntersect(x, y, nextCheckpointX, nextCheckpointY, 700, 900);
        //bool podNearOpponent = circleIntersect(x, y, opponentX, opponentY, 600, 600);
        bool isLastIndex = currentMapIndex == (mapCheckPoints.size() - 1);

        if (!mapAllCheckPoints) {
            // Check if next checkpoint is starting checkpoint
            if (mapCheckPoints.size() > 1 && nextCheckpointX == mapCheckPoints[0].x && nextCheckpointY == mapCheckPoints[0].y) {
                mapAllCheckPoints = true;
            } else if (nextCheckpointX != mapCheckPoints[mapCheckPoints.size() - 1].x && nextCheckpointY != mapCheckPoints[mapCheckPoints.size() - 1].y) {
                // Store next checkpoint on arrival to checkpoint
                mapCheckPoints.insert(pair<unsigned int, CheckPointCoordinates>(mapCheckPoints.size(), {nextCheckpointX, nextCheckpointY}));
            }
        } else {
            // If checkpoint is last of the lap, reset to first checkpoint
            if (isLastIndex) {
                if (circleIntersect(x, y, mapCheckPoints[0].x,  mapCheckPoints[0].y, 400, 600)) {
                    currentMapIndex = 0;
                }
            } else {
                if (circleIntersect(x, y, mapCheckPoints[currentMapIndex + 1].x,  mapCheckPoints[currentMapIndex + 1].y, 400, 600)) {
                    currentMapIndex += 1;
                }
            }
        }

        // Run race as usual            
        if (podFacingOppositeDirection) {
            thrust = 0;
        } else if (podCheckPointIntersection) {
            // Slow down when reaching checkpoint
            thrust -= 1;
        } else {
            thrust = 100;
        }

        // Use boost when pod to checkpoint is a straight line of more than 4500 units to avoid shooting over checkpoint
        if (!useBoost && (nextCheckpointDist >= 4500 && nextCheckpointAngle == 0)) {
            cout << nextCheckpointX << " " << nextCheckpointY << " BOOST" << endl;
            useBoost = true;
        } else if (mapAllCheckPoints && thrust == 85) {
            // Turn pod to next checkpoint direction
            if (isLastIndex) {
                cout << mapCheckPoints[0].x << " " << mapCheckPoints[0].y << " " << thrust << endl;
            } else {
                cout << mapCheckPoints[currentMapIndex + 1].x << " " << mapCheckPoints[currentMapIndex + 1].y << " " << thrust << endl;
            }
        } else {
            cout << nextCheckpointX << " " << nextCheckpointY << " " << thrust << endl;
        }
    }
}
```