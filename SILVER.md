#### Decision
1. Coming to Silver league, my first thought was to amend the code and utilize `nextCheckpointDist` instead of `circleIntersect`, and estimate the distance to slow down before checkpoint is reached. As it turns out, I realize that my code was having a lot of bugs and it wasn't working as I plan but somehow I got lucky and got through Bronze league, so I started refactoring and getting my logic tested to be able to ensure that the concept I had in mind was being implemented correctly. The idea was still simple, in the first lap when I am storing all the checkpoints, I will reduce speed when the distance is at about 20 percent of the full distance between each checkpoints, then if I had the next checkpoint, start getting the pod to aim for the next checkpoint. For the most part, the implementation was correct but not sufficient to get through the league.
2. I started thinking about the angles from each checkpoint and so I would be able to determine just how much thrust I need to adjust depending on the angle since some checkpoints were relatively straight to each other. The slowdown distance was still required since the pod may be coming at a steep angle to the checkpoint causing it to miss and rotate around the checkpoint which is not idle. The angle implementation worked out quite well but adjusting the distance to slow down and the right amount of thrust value was still not idle since the pod can be coming in too quickly or sometimes due to the angle causing the pod to either miss the checkpoint entirely.
3. I have not successfully done this but the idea is to dynamically calculate the thrust value depending on the distance, angle to checkpoint and angle between checkpoint. Also, I needed to be able to get the right offset so that on turning the pod, it will be in the right direction to the next checkpoint instead of doing a counter rotation that causes the pod to be extremely slow.

#### Time Spent
1. 6 hours
2. 10 hours
3. 8 hours

#### Final Code
```c++
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <queue>
#include <cmath>

using namespace std;

#define CONST_LONG_DISTANCE 6500
#define CONST_PI 3.14159265

struct GameVector
{
    int x;
    int y;
};

struct CheckpointCoordinates
{
    int x;
    int y;
    GameVector vecToNextCheckpoint;
};

enum TurningType
{
    Shallow,
    Mild,
    Aggressive,
};

GameVector calculateVector(CheckpointCoordinates coordinate1, CheckpointCoordinates coordinate2)
{
    GameVector result;
    result.x = coordinate2.x - coordinate1.x;
    result.y = coordinate2.y - coordinate1.y;
    return result;
}

TurningType getTurningType(GameVector v1, GameVector v2)
{
    int dot = (v1.x * v2.x) + (v1.y * v2.y);
    int det = (v1.x * v2.y) - (v1.y * v2.x);
    int vectorAngle = atan2(det, dot) * 180 / CONST_PI;
    if (vectorAngle >= -45 && vectorAngle <= 45) {
        cerr << "Shallow turn" << endl;
        return Shallow;
    } else if (vectorAngle >= -90 && vectorAngle <= 90) {
        cerr << "Mild turn" << endl;
        return Mild;
    } else {
        cerr << "Aggressive turn" << endl;
        return Aggressive;
    }
}

// Get next checkpoint and put it back of queue
CheckpointCoordinates getNextCheckpoint(queue<CheckpointCoordinates> &qCheckpoints) {
    CheckpointCoordinates result = qCheckpoints.front();
    qCheckpoints.pop();
    qCheckpoints.push(result);
    return result;
}

void updateOnNewCheckpoint(int &targetX, int &targetY, int &checkpointsPassed, int &distanceToSlowDown, CheckpointCoordinates newCheckpoint, int nextCheckpointDist)
{
    targetX = newCheckpoint.x;
    targetY = newCheckpoint.y;
    checkpointsPassed += 1;
    distanceToSlowDown = max(500, static_cast<int>(nextCheckpointDist * 0.2));
    cerr << "Distance to slow down " << distanceToSlowDown << endl;
}

// Credit: https://www.geeksforgeeks.org/check-two-given-circles-touch-intersect/
bool circleIntersect(int x1, int y1, int x2, int y2, int r1, int r2)
{
    int distSq = (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2);
    int radSumSq = (r1 + r2) * (r1 + r2);
    if (distSq > radSumSq)
        return false;
    else
        return true;
}

int main()
{
    queue<CheckpointCoordinates> qCheckpoints;
    bool useBoost = false;
    bool useShield = false;
    CheckpointCoordinates currentCheckpoint;
    int thrust = 100;
    int distanceToSlowDown;
    TurningType turnHandling = Mild;
    int totalCheckpoints = -1;
    int countCheckpointsPassed = 0;
    int targetX;
    int targetY;

    // game loop
    while (1) {
        int x;
        int y;
        int nextCheckpointX; // x position of the next check point
        int nextCheckpointY; // y position of the next check point
        int nextCheckpointDist; // distance to the next checkpoint
        int nextCheckpointAngle; // angle between your pod orientation and the direction of the next checkpoint
        cin >> x >> y >> nextCheckpointX >> nextCheckpointY >> nextCheckpointDist >> nextCheckpointAngle; cin.ignore();
        int opponentX;
        int opponentY;
        cin >> opponentX >> opponentY; cin.ignore();

        bool isLastCheckpoint = (totalCheckpoints != -1) && countCheckpointsPassed == (totalCheckpoints - 1);

        if (totalCheckpoints == -1) {
            // Check if next checkpoint is starting checkpoint or new checkpoint
            if ((qCheckpoints.size() == 0) || (nextCheckpointX != qCheckpoints.back().x && nextCheckpointY != qCheckpoints.back().y)) {
                if (qCheckpoints.size() != 0) {
                    // Calculate checkpoint vector
                    qCheckpoints.back().vecToNextCheckpoint = calculateVector(qCheckpoints.back(), {nextCheckpointX, nextCheckpointY});
                }
                // Store next checkpoint
                qCheckpoints.push({nextCheckpointX, nextCheckpointY});
                currentCheckpoint = qCheckpoints.back();
            } else if (qCheckpoints.size() > 1 && nextCheckpointX == qCheckpoints.front().x && nextCheckpointY == qCheckpoints.front().y) {
                qCheckpoints.back().vecToNextCheckpoint = calculateVector(qCheckpoints.back(), {nextCheckpointX, nextCheckpointY});
                totalCheckpoints = countCheckpointsPassed * 3;
                currentCheckpoint = getNextCheckpoint(qCheckpoints);
            }
            updateOnNewCheckpoint(targetX, targetY, countCheckpointsPassed, distanceToSlowDown, currentCheckpoint, nextCheckpointDist);
        } else {
            // Update checkpointIndex on change of checkpoint
            if (nextCheckpointX != currentCheckpoint.x && nextCheckpointY != currentCheckpoint.y) {
                turnHandling = getTurningType(currentCheckpoint.vecToNextCheckpoint, qCheckpoints.front().vecToNextCheckpoint);
                currentCheckpoint = getNextCheckpoint(qCheckpoints);
                updateOnNewCheckpoint(targetX, targetY, countCheckpointsPassed, distanceToSlowDown, currentCheckpoint, nextCheckpointDist);
            }
        }

        if (nextCheckpointAngle >= 100 || nextCheckpointAngle <= -100) {
            thrust = 0;
        } else if (!isLastCheckpoint && nextCheckpointDist <= distanceToSlowDown) {
            switch (turnHandling)
            {
            case Aggressive:
                thrust = 20;
                break;
            case Mild:
                thrust = 50;
                break;
            default:
                thrust = 90;
                break;
            }
            if (totalCheckpoints != -1) {
                targetX = qCheckpoints.front().x;
                targetY = qCheckpoints.front().y;
            }
        } else if (nextCheckpointAngle >= 70 || nextCheckpointAngle <= -70) {
            thrust = 50;
        } else if (nextCheckpointAngle >= 30 || nextCheckpointAngle <= -30) {
            thrust = 90;
        } else {
            thrust = 100;
        }

        if (!useBoost && nextCheckpointDist >= CONST_LONG_DISTANCE && nextCheckpointAngle <= 10 && nextCheckpointAngle >= -10) {
            cout << targetX << " " << targetY << " BOOST" << endl;
            cerr << "Use boost" << endl;
            useBoost = true;
        } else if (totalCheckpoints != -1 && !useShield && circleIntersect(x, y, opponentX, opponentY, 600, 600)) {
            cout << targetX << " " << targetY << " SHIELD" << endl;
            cerr << "Use shield" << endl;
            useShield = true;
        } else {
            cout << targetX << " " << targetY << " " << thrust << endl;
        }
    }
}
```