# Level 2

#### Decision
1. Testing the initial code provided did not win the race (obviously) so I first increased/reduced thrust value when the angle was more than 90 or less than -90 degress but it failed.
2. I noticed the boss was using the same thrust values as the initial code provided so I reduced the angle values from 90 to 45 so that the pod can turn without too much time and accelerate when the destination is more or less in a more straight line.

#### Time Spent
1. 1 min
2. 1 min

#### Final Code
```c++
int thrust;
// Write an action using cout. DON'T FORGET THE "<< endl"
// To debug: cerr << "Debug messages..." << endl;
if (nextCheckpointAngle > 45 || nextCheckpointAngle < -45) {
    thrust = 0;
} else {
    thrust = 100;
}

cout << nextCheckpointX << " " << nextCheckpointY << " " << thrust << endl;
```
# Level 3

#### Decision
1. I played around with various degrees to help me achieve the win without utilizing boost and felt that increasing the comparison from 45 to 100 helped my pod accelerate through turns in an efficient manner.
2. I then added a condition if the distance to next checkpoint was == 4500 and the degree was 0, I would utilize the boost. The idea here is that if the point of my pod to destination was a straight line, utilizing boost will help reach the destination quicker and not be wasted by the pod's rotation.

#### Time Spent
1. 16 mins
2. 2 mins

#### Final Code
```c++
int thrust;
// Write an action using cout. DON'T FORGET THE "<< endl"
// To debug: cerr << "Debug messages..." << endl;
if (nextCheckpointAngle > 100 || nextCheckpointAngle < -100) {
    thrust = 0;
} else {
    thrust = 100;
}

if (nextCheckpointDist == 4500 && nextCheckpointAngle == 0) {
    cout << nextCheckpointX << " " << nextCheckpointY << " BOOST" << endl;
} else {
    cout << nextCheckpointX << " " << nextCheckpointY << " " << thrust << endl;
}
```